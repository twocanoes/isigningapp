//
//  TokenManager.swift
//  SmartCard Utility
//
//  Created by Timothy Perfitt on 11/2/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

import CryptoTokenKit



class TokenManager: NSObject {
    class func removeAllConfigs(configID:String) {
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations

        if let configurationInfo = driverConfig[configID]{
            let tokenConfigs = configurationInfo.tokenConfigurations

            for (key,_) in tokenConfigs {
                configurationInfo.removeTokenConfiguration(for: key)
            }

        }

    }

    class func removeToken(configID:String, tokenIdentifer: String) {
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations
        let configurationInfo = driverConfig[configID]
        configurationInfo?.removeTokenConfiguration(for: tokenIdentifer)

    }
    @objc class func insertConfig(configName:String ) {

        var keyItems = [TKTokenKeychainItem]()
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations
        print("Driver Configs: \(driverConfig.description)")

        if let tokenConfig = driverConfig["com.twocanoes.iSigningApp.iSigningAppToken"] {
            print(tokenConfig.tokenConfigurations)
            let newConfig = tokenConfig.addTokenConfiguration(for: configName)
            newConfig.configurationData=keyFromP12()
            let constraint = "NONE"

            let constraints : [ NSNumber : TKTokenOperationConstraint] = [
                TKTokenOperation.readData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
                TKTokenOperation.decryptData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
                TKTokenOperation.none.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
                TKTokenOperation.performKeyExchange.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
                TKTokenOperation.signData.rawValue as NSNumber: constraint as TKTokenOperationConstraint
            ]

            let p12CertData = certFromP12()

                if let currCert = p12CertData,
                   let certificate = SecCertificateCreateWithData(nil, currCert as CFData){

                    let keychainCertificate = TKTokenKeychainCertificate.init(certificate: certificate, objectID: currCert)
                    keychainCertificate?.label = "Certificate"
                    keyItems.append(keychainCertificate!)
                    if let certItemKey = TKTokenKeychainKey.init(certificate: certificate, objectID: "key") {
                        certItemKey.canSign = true
                        certItemKey.canDecrypt = true
                        certItemKey.canPerformKeyExchange = true
                        certItemKey.isSuitableForLogin = true
                        certItemKey.label = "keylabel"
                        certItemKey.constraints = constraints
                        keyItems.append(certItemKey)
                    }
                }


            newConfig.keychainItems = keyItems

        }

        print("Done with insertion")
    }
    class func keyFromP12() -> Data?{
        var items: CFArray?
        let path = Bundle.main.path(forResource: "id", ofType: "p12")

        let certificateData = try! Data.init(contentsOf: URL.init(fileURLWithPath: path!), options: Data.ReadingOptions.uncached)


        let options = [kSecImportExportPassphrase: "twocanoes" as AnyObject] as [String: Any]

        let err = SecPKCS12Import(certificateData as CFData, options as CFDictionary, &items)

        if err != errSecSuccess || items == nil {
            return nil            }

        let itemArray = items! as Array

        let identity = itemArray.first!["identity"] as! SecIdentity

        var privateKey: SecKey?
        var certificate: SecCertificate?

        _ = SecIdentityCopyPrivateKey(identity, &privateKey)
        _ = SecIdentityCopyCertificate(identity, &certificate)
        if let privKey = privateKey,
           let privKeyData = SecKeyCopyExternalRepresentation(privKey, nil){
            return privKeyData as Data
            
        }
        return nil
    }
    class func certFromP12() -> Data?{
        var items: CFArray?
        let path = Bundle.main.path(forResource: "id", ofType: "p12")
        
        let certificateData = try! Data.init(contentsOf: URL.init(fileURLWithPath: path!), options: Data.ReadingOptions.uncached)
        
        let options = [kSecImportExportPassphrase: "twocanoes" as AnyObject] as [String: Any]

        let err = SecPKCS12Import(certificateData as CFData, options as CFDictionary, &items)

        if err != errSecSuccess || items == nil {
            return nil            }

        let itemArray = items! as Array

        let identity = itemArray.first!["identity"] as! SecIdentity

        var privateKey: SecKey?
        var certificate: SecCertificate?

        _ = SecIdentityCopyPrivateKey(identity, &privateKey)
        _ = SecIdentityCopyCertificate(identity, &certificate)
        if let certificate = certificate {
            let certData = SecCertificateCopyData(certificate)
            return certData as Data

        }
        return nil
    }

}
