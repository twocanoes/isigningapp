//
//  sharedLogger.swift
//  Smart Card Utility
//
//  Created by Timothy Perfitt on 11/1/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

import os
import Foundation
class Logging: NSObject {
    @objc static let sharedLogger = Logging()
    var isConnect = false
    @objc func printLog(message: String) {

        print("\(message)\n")

        var currentLog = ""

        let dateString = ISO8601DateFormatter.string(from:Date(), timeZone:NSTimeZone.local, formatOptions: ISO8601DateFormatter.Options.withInternetDateTime)

        currentLog.append(String(format: "%@: %@\n", dateString,message))
        print(currentLog)

        let pivLog = OSLog(subsystem: "com.twocanoes.logger", category: "piv")

        DispatchQueue.main.async {
            os_log("%{public}s",log:pivLog,message)
        }
    }
    func Mark(
                 file: String = #file, line: Int = #line, function: String = #function ) {

        let date = Date()

        let comp = file.components(separatedBy: "/")
        if let lastPart = comp.last{
            printLog(message:"\(date) FILE:\(lastPart) LINE:\(line) FUNCTION:\(function)")

        }

    }

}
