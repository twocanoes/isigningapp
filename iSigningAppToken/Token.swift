//
//  Token.swift
//  iSigningAppToken
//
//  Created by Timothy Perfitt on 1/17/22.
//

import CryptoTokenKit

class Token: TKToken, TKTokenDelegate {
    func createSession(_ token: TKToken) throws -> TKTokenSession {
        Logging.sharedLogger.Mark()
        return TokenSession(token:self)
    }

}
