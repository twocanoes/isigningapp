//
//  TokenSession.swift
//  iSigningAppToken
//
//  Created by Timothy Perfitt on 1/17/22.
//

import CryptoTokenKit

class TokenSession: TKTokenSession, TKTokenSessionDelegate {

    func tokenSession(_ session: TKTokenSession, beginAuthFor operation: TKTokenOperation, constraint: Any) throws -> TKTokenAuthOperation {
        Logging.sharedLogger.Mark()
        // Insert code here to create an instance of TKTokenAuthOperation based on the specified operation and constraint.
        // Note that the constraint was previously established when creating token configuration with keychain items.
        return TKTokenPasswordAuthOperation()
    }
    
    func tokenSession(_ session: TKTokenSession, supports operation: TKTokenOperation, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) -> Bool {
        Logging.sharedLogger.Mark()
        return (algorithm.isAlgorithm(SecKeyAlgorithm.rsaSignatureRaw))


    }
    
    func tokenSession(_ session: TKTokenSession, sign dataToSign: Data, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) throws -> Data {

        Logging.sharedLogger.Mark()
        let driverConfigs = TKTokenDriver.Configuration.driverConfigurations
        let driverConfig = driverConfigs["com.twocanoes.iSigningApp.iSigningAppToken"]
        let tokenConfigDict = driverConfig?.tokenConfigurations
        let config = tokenConfigDict!["iSigningAppConfig2"]

        let keyData = config?.configurationData

        let attrs = [
            kSecAttrKeyType: kSecAttrKeyTypeRSA,
            kSecAttrKeyClass: kSecAttrKeyClassPrivate
        ]

        if let privKeyData = keyData,
           let privKey = SecKeyCreateWithData(privKeyData as CFData, attrs as CFDictionary, nil) {
            var error: Unmanaged<CFError>?
            if let signature = SecKeyCreateSignature(privKey, .rsaSignatureRaw, dataToSign as CFData, &error) {
                return signature as Data

            }

            else {
                throw TKError(.corruptedData)
            }
        }
        return Data()
    }
    
    func tokenSession(_ session: TKTokenSession, decrypt ciphertext: Data, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) throws -> Data {
        var plaintext: Data?
        Logging.sharedLogger.Mark()
        // Insert code here to decrypt the ciphertext using the specified key and algorithm.
        plaintext = nil
        
        if let plaintext = plaintext {
            return plaintext
        } else {
            // If the operation failed for some reason, fill in an appropriate error like objectNotFound, corruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            throw NSError(domain: TKErrorDomain, code: TKError.Code.authenticationNeeded.rawValue, userInfo: nil)
        }
    }
    
    func tokenSession(_ session: TKTokenSession, performKeyExchange otherPartyPublicKeyData: Data, keyObjectID objectID: Any, algorithm: TKTokenKeyAlgorithm, parameters: TKTokenKeyExchangeParameters) throws -> Data {
        var secret: Data?
        Logging.sharedLogger.Mark()
        // Insert code here to perform Diffie-Hellman style key exchange.
        secret = nil
        
        if let secret = secret {
            return secret
        } else {
            // If the operation failed for some reason, fill in an appropriate error like objectNotFound, corruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            throw NSError(domain: TKErrorDomain, code: TKError.Code.authenticationNeeded.rawValue, userInfo: nil)
        }
    }
}
