//
//  TokenDriver.swift
//  iSigningAppToken
//
//  Created by Timothy Perfitt on 1/17/22.
//

import CryptoTokenKit

class TokenDriver: TKTokenDriver, TKTokenDriverDelegate {

    func tokenDriver(_ driver: TKTokenDriver, tokenFor configuration: TKToken.Configuration) throws -> TKToken {
        Logging.sharedLogger.Mark()
        return Token(tokenDriver: self, instanceID: configuration.instanceID)
    }

}
