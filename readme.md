# iSigningApp sample app #

## Overview ##
iSigning App is a simple iOS for show how to insert a CTK token into iOS. It contains a P12 certificate and private key embedded in the app. When Insert is pressed, the certificate and private key is read from the p12. The certificate is inserted into the keychain using keychainItems of a TKTokenConfig. The private key is added as configurationData. This is horribly insecure but it does illustrate a simple way to insert a certificate and sign it with a key. The key is usually secure off device, in a secure enclave, on a server, in a smart card or other secure storage. Please do not distribute apps with embedded p12s and passwords. 


## How to use it ##

1. Tap insert in the main screen. It should show the common name of the cert. If it has a message about rebooting, sigh and reboot. This is an [apple bug](https://twocanoes.com/knowledge-base/fb9849545-ios-ipados-ctk-extension-stops-working-when-installing-via-testfight-until-reboot/).

2. See the certificate is inserted and can be used by going to a test server like [https://server.cryptomix.com/secure/](https://server.cryptomix.com/secure/)

3. Open Settings and see that the cert is available to use in 802.1x and VPN configs.

4. See example video here:

## Building ##

Make sure to replace all instances of com.twocanoes with your team prefix. Otherwise it won't work.

## Movie ##

[See it in action](https://www.dropbox.com/s/6qvmqvr7fdgcgi5/RPReplay_Final1651550283.mp4?dl=0).