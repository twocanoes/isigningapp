//
//  ViewController.swift
//  iSigningApp
//
//  Created by Timothy Perfitt on 1/17/22.
//

import UIKit
import CryptoTokenKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        readConfigTapped(self)

    }

    @IBAction func removeAll(_ sender:Any){
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations

        if let configurationInfo = driverConfig["com.twocanoes.iSigningApp.iSigningAppToken"] {
            let tokenConfigs = configurationInfo.tokenConfigurations

            for (key,_) in tokenConfigs {
                configurationInfo.removeTokenConfiguration(for: key)


            }

        }
        readConfigTapped(self)

    }


    @IBAction func insertConfigTapped(_ sender: Any) {

        TokenManager.insertConfig(configName: "iSigningAppConfig2")
        readConfigTapped(self)


    }

    @IBAction func readConfigTapped(_ sender: Any) {

        var item: CFTypeRef?
        textView.text=""

        let query = [kSecClass: kSecClassIdentity,
                kSecMatchLimit: kSecMatchLimitAll,
                 kSecReturnRef: kCFBooleanTrue as Any,
          kSecReturnAttributes: kCFBooleanFalse as Any] as [CFString : Any]
        let sanityCheck = SecItemCopyMatching(query as CFDictionary,&item)

        if (sanityCheck != noErr) {
             return ;
        }
        let identityArray = item as! Array<SecIdentity>

        if identityArray.count==0 {
            textView.text="No certs found. reboot."
            return
        }
        var certInfo=Date().description.appending("\n")
        for i in identityArray {
            var cert:SecCertificate?
            SecIdentityCopyCertificate(i, &cert)
            if let cert = cert {
                var commonName:CFString?
                let summary=SecCertificateCopySubjectSummary(cert)!  as String
                SecCertificateCopyCommonName(cert, &commonName)
                certInfo=certInfo.appending(summary).appending("\n")

            }

        }
        textView.text=certInfo

    }
}

